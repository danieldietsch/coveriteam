<!--
This file is part of CoVeriTeam,
a tool for on-demand composition of cooperative verification systems:
https://gitlab.com/sosy-lab/software/coveriteam

SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# CoVeriTeam Remote

CoVeriTeam allows running tools on the user's machine without the need to manually install them.
Please read [this](../README.md) for more details.

CoVeriTeam Remote allows executing a CoVeriTeam program on a server.
We have developed a REST based service to support this.
The user can execute the program on the verifier cloud by simply adding the flag `--remote`
to the command.
For example,
```bash
../bin/coveriteam verifier-C.cvt \
  --input ver_path=../actors/cpa-seq.yml \
  --input program_path=../../sv-benchmarks/c/ldv-regression/test02.c \
  --input specification_path=../../sv-benchmarks/c/properties/unreach-call.prp \
```
 will execute the verifier CPAChecker on the program `test02.c` from sv-benchmarks
 to check reach safety. 
 Just by adding `--remote` we can execute this program on the verifier cloud through 
 our web service.
 ```bash
 ../bin/coveriteam verifier-C.cvt \
   --input ver_path=../actors/cpa-seq.yml \
   --input program_path=../../sv-benchmarks/c/ldv-regression/test02.c \
   --input specification_path=../../sv-benchmarks/c/properties/unreach-call.prp \
   --remote
 ```
  

## Examples for SV Comp 2021
As a help we have created a [file](../examples/remote_examples.txt)  containing an example command 
for each verification tool that participated in the software verification competition 2020.
This command can be used to execute the tool on the verifier cloud.
Obviously, you can try it out with other files and properties also from the sv-benchmarks repository.

As an example, lets consider the command
```bash
../bin/coveriteam verifier-C.cvt \
  --input ver_path=../actors/cpa-seq.yml \
  --input program_path=../../sv-benchmarks/c/ldv-regression/test02.c \
  --input specification_path=../../sv-benchmarks/c/properties/unreach-call.prp \
  --remote
```

This command executes CoVeriTeam remotely on `verifier-C.cvt`.
`verifier-C.cvt` is an example CoVeriTeam program which instantiates a verifier based on an actor definition YAML file.
Then this verifier is executed on a program and a property.
Following is the code for `verifier-C.cvt`:

```bash
// Create verifier from external-actor definition file
ver = ActorFactory.create(ProgramVerifier, ver_path);

// Print type information of the created ProgramVerifier
print(ver);

// Prepare example inputs
prog = ArtifactFactory.create(Program, program_path);
spec = ArtifactFactory.create(BehaviorSpecification, specification_path);
inputs = {'program':prog, 'spec':spec};

// Execute the verifier on the inputs
res = execute(ver, inputs);
print(res);
```

We have created actor definitions for the verification tools that participated in
the software verification competition 2020.
[Actors](../actors/) contains these actor definitions.
For example actor definition for the participant `cpa-seq` looks like this:
```YAML
imports:
  - !include verifier_resource.yml
actor_name: cpa-seq
toolinfo_module: "cpachecker.py"
options: ['-svcomp20', '-heap', '10000M', '-benchmark', '-timelimit', '900 s']
archive:
  location: "https://gitlab.com/sosy-lab/sv-comp/archives-2020/-/raw/master/2020/cpa-seq.zip"
  spdx_license_identifier: "Apache-2.0"
format_version: '1.1'
```
First we import `verifier_resource.yml` which sets the resource limits for the execution.
Then we specify the name of the actor, then its tool info module.
Tool info module can be specified using a url or the name of the tool info module file.
Then we specify the options which are used by the tool in the competition, and then the archive location.
At the moment the archives are pointing to the archives of the 2020's competition.
These will be updated to 2021 soon. And then we specify the license which is an optional field.


## Current Restrictions
We have imposed the following restrictions to execute a verifier using the remote feature:
1. The actor definition (yml file) must be in the main branch of the [CoVeriTeam repository](https://gitlab.com/sosy-lab/software/coveriteam)
2. The program and property files must be in the main branch of the [sv-benchmarks repository](https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks)
3. Only those tool info modules can be used which are either released with the benchexec, or are in the main branch of the [benchexec repository](https://github.com/sosy-lab/benchexec).
   A user can use a url to specify a tool info module in the actor definition YAML file, instead of the name of the tool info module.
   
A user can execute a verifier using CoVeriTeam Remote from the directory with the directory in which `sv-benchmarks` has been cloned.


